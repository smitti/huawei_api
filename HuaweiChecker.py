from Huawei import Huawei


# Main
myHua = Huawei("admin","YOUR_PASSWORD","ROUTERS_IP")

#prints the basic information for traffic statistics
myHua.get_information()

#gets the download/upload since the last reconnect
currentDownload = myHua.get_current_download_MB()
currentUpload = myHua.get_current_upload_MB()
print( currentDownload+ " MB / " + currentUpload + " MB")
