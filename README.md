# README #

This README gives you a short info how to run the script. As I could not find any useful API which works like I needed it, I've created this small and simple one and want to share it with you.
My usage: I am running it on my rpi and monitor some of the Huawei Modem's parameter with RPiMonitor.

### What is this repository for? ###

* To access information from the Huawei Router. In my case it is a B315s-22, but it should also work with others
* Version: 1

### How to set it up? ###

* install python3
* download the file Huawei.py
* import the class Huawei e.g. in a file which is in the same directory as the Huawei.py see the sample HuaweiPhecker.py

### Additional Info and Thanks ###

* I kept it pretty simple, so it is easier to customize it
* Thanks to the developer of the tool "LTEWatch" which I decompiled and checked how to authenticate to the router
