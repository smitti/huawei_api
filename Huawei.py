import requests
import xml.etree.ElementTree as ET
import hashlib
import base64


class Huawei:
    """
    Main Class which does the authentication and gets the Information from the Huawei Router
    """

    def __init__(self, username, password, ip="192.168.8.1"):
        """
        Initialize a Huawei Router Object defined by login username, password and the IP to the router can also be specified. If not, default IP is taken

        Arguments:

        * username (string): Username usually is "admin" and is not directly specified when login to the router via the webinterface
        * password (string): The defined password to login to the routers webinterface
        * ip (string): IP-Address ot the Router. If not specified the default value 192.168.8.1 is taken
        """
        self.token = ""
        self.sessID = ""
        self.loggedin = False
        self.ip = ip
        self.general_header = {
            "Host": self.ip,
            "__RequestVerificationToken": "",
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0",
            "Accept": "*/*",
            "Accept-Encoding": "gzip,deflate",
            "Accept-Language": "de-AT,de-DE;q=0.7,de;q=0.3",
            "Cookie": ""
        }

        self.initial_request()
        self.login(username, password)

    def initial_request(self):
        """Initial_request does a get request to the huawei
        modem to get the verification token and the session ID"""

        url = 'http://' + self.ip + '/api/webserver/SesTokInfo'

        # No payload (data) required for this request
        r = requests.get(url, data="", headers=self.general_header)
        # print(r.content)
        # print("------------")
        tree = ET.ElementTree(ET.fromstring(r.content.decode(
            "utf-8")))  # r.content is a byte array. Convert it to a string with .decode("utf-8"). ET.fromstring returns the root element

        self.token = tree.find("TokInfo").text
        self.sessID = tree.find("SesInfo").text

        # print("Initial verification Token: " + self.token + "\n")
        # print("Initial Session ID: " + self.sessID + "\n")

    def login(self, user, pw):
        """
        Huawei api authentication:

        SHA256(Password_as_Byte) --> stringToBase64 --> Bytes --> .decode('ascii') --> hashedPW_as_string
        SHA256(username + hashedPW_as_string + Token) --> stringToBase64 --> Bytes --> .decode('ascii') --> finalPW_as_string

        1. The Login Password gets SHA256 hashed and afterwards Base64 encoded
        2. Username as string combined with the above result (as string) and the VerificationToken (as string) --> (username + hashed_and_base64end_PW + verficationtoken)
        3. This combined string is again hashed SHA256 and afterwards Base64 encoded

        Example:
            noBase64:
            str{80a041e818a5ab7dd70be938f140ca49b867fa798f4b5b7e04dad5600dad6eb0}
            base64:
            str{ODBhMDQxZTgxOGE1YWI3ZGQ3MGJlOTM4ZjE0MGNhNDliODY3ZmE3OThmNGI1YjdlMDRkYWQ1NjAwZGFkNmViMA==}

        """

        url = 'http://' + self.ip + '/api/user/login'
        hash_obj = hashlib.sha256(pw.encode())  # sha256 requires byte array --> encode() to achieve this
        hashedpw = hash_obj.hexdigest()  # hash it and return a string
        # print("noBase64")
        # print(hashedpw)
        hashedpw = self.stringtobase64(hashedpw).decode(
            'ascii')  # .decode ascii to get the result b'hashedpw' from a byte to a normal string to concat it with username and token in the next step
        # print("base64")
        # print(hashedpw)

        hash_obj = hashlib.sha256((user + hashedpw + self.token).encode())
        finalpw = hash_obj.hexdigest()  # has it
        finalpw = self.stringtobase64(finalpw).decode('ascii')

        payload = "<?xml version \"1.0\" encoding\"UTF-8\"?><request><Username>admin</Username><Password>" + finalpw + "</Password><password_type>4</password_type></request>"
        # print(payload)
        r = requests.post(url, data=payload, headers=self.getCurrentHeader())
        # print("Login Response CONTENT: ")
        # print(r.content) # OUTPUT: Login Response CONTENT: b'<?xml version="1.0" encoding="UTF-8"?><response>OK</response>'
        # print("Login Respones HEADER")
        # print(r.headers)
        ''' After successfull authentication, router responses with <response>OK</response> and also a new SessionID and Verification Token. From now on requests need to contain the new values for Token and SessionID'''

        tree = ET.ElementTree(ET.fromstring(r.content.decode("utf-8")))
        if tree.getroot().text == "OK":
            print(tree.getroot().text)
            self.loggedin = True

            '''Response Header contains 2 Verfication Tokens. In the tests it always worked when just using the Verification Token One'''
            # print("RequestVerificationTokenOne")
            # print(r.headers['__requestverificationtokenone'])
            # print("RequestVerificationTokenTwo")
            # print(r.headers['__requestverificationtokentwo'])
            # print("SessionID")
            # print(r.headers['set-cookie'])

            self.token = r.headers['__requestverificationtokenone']
            self.sessID = r.headers['set-cookie']

    def get_information(self):
        path = '/api/device/information'
        xml = self.general_request(path)
        print(xml)

    def get_current_download_MB(self):
        total_download = self.get_traffic_stat()
        return self.ToMBString(float(
            total_download["CurrentDownload"]))  # convert string to float for dividing and then back to string again

    def get_current_upload_MB(self):
        total_download = self.get_traffic_stat()
        return self.ToMBString(float(
            total_download["CurrentUpload"]))  # convert string to float for dividing and then back to string again

    def get_traffic_stat(self):
        path = "/api/monitoring/traffic-statistics"
        xml = self.general_request(path)
        print(xml)

        root = ET.XML(xml)
        xmldict = XmlDictConfig(root)

        '''
        <CurrentConnectTime>23626</CurrentConnectTime>
        <CurrentUpload>156219654</CurrentUpload> upload from router reconnect in bytes
        <CurrentDownload>2320279902</CurrentDownload> download from router reconnect in bytes
        <CurrentDownloadRate>0</CurrentDownloadRate> current download rate in bit
        <CurrentUploadRate>0</CurrentUploadRate> current download rate in bit
        <TotalUpload>78561857963</TotalUpload> total upload byte
        <TotalDownload>1241665068497</TotalDownload> total download in byte
        <TotalConnectTime>34892497</TotalConnectTime>
        <showtraffic>1</showtraffic>

        Returned dict: {'CurrentUploadRate': '4304', 'showtraffic': '1', 'TotalDownload': '1247572002134', 'CurrentConnectTime': '14515', 'TotalConnectTime': '35055943', 'CurrentUpload': '125272254', 'CurrentDownload': '941726199', 'TotalUpload': '79352180697', 'CurrentDownloadRate': '4352'}

        '''

        return xmldict

    def general_request(self, path):
        url = 'http://' + self.ip + path
        if self.loggedin:
            r = requests.get(url, data="", headers=self.getCurrentHeader())
            return r.content.decode("utf-8")
        else:
            print("Not Logged in")

    def stringtobase64(self, s):
        return base64.b64encode(s.encode('utf-8'))

    def base64tostring(self, b):
        return base64.b64decode(b).decode('utf-8')

    def getCurrentHeader(self):
        """
        original idea of storing token and sessionID in the header dict as instance variable does not work, as token and sessionID values are not updated, just the value at dict initialization is taken
        this function adds the current value of token and sessionID to the general header, defined in the instance variable self.header and returns it
        """

        tmp = self.general_header.copy()
        header_update = {"__RequestVerificationToken": self.token, "Cookie": self.sessID}
        tmp.update(header_update)  # which returns None since it mutates z
        return tmp  # in python >3.5 the function would be: return {**self.mydict,**header_update}

    def ToMBString(self, value):
        return str(int(value / 1024 / 1024))


# XML Stuff from http://code.activestate.com/recipes/410469-xml-as-dictionary/
class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)


class XmlDictConfig(dict):
    '''
    Example usage:

    >>> tree = ElementTree.parse('your_file.xml')
    >>> root = tree.getroot()
    >>> xmldict = XmlDictConfig(root)

    Or, if you want to use an XML string:

    >>> root = ElementTree.XML(xml_string)
    >>> xmldict = XmlDictConfig(root)

    And then use xmldict for what it is... a dict.
    '''

    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})
